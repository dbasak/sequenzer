package com.sequenzer;

import com.sequenzer.route.*;

import static spark.Spark.get;

public class SequenceService {

    private SequenceController theSequenceController;

    public SequenceService(final SequenceController aSequenceController)
    {
        theSequenceController = aSequenceController;
    }

    public void start()
    {
        get(new CreateRoute("/sequence/create", theSequenceController));
        get(new CreateFromTillRoute("/sequence/create/:from/:till", theSequenceController));
        get(new CurrentRoute("/sequence/current", theSequenceController));
        get(new GetSequenceRoute("/sequence/get", theSequenceController));
        get(new LastSequenceRoute("/sequence/last", theSequenceController));
        get(new FullSequenceRoute("/sequence/all", theSequenceController));
    }
}
