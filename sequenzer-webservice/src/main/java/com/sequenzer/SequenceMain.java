package com.sequenzer;

import java.util.*;

public class SequenceMain {
    public static void main(String[] args)
    {
        new SequenceService(new SequenceController
                                    (new SequenceStructure(new HashSet<Long>(), 0L, 0L))).start();
    }
}
