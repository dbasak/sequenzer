package com.sequenzer.route;

import com.sequenzer.*;
import spark.*;

public class GetSequenceRoute extends Route {
    private final SequenceController theSequenceController;

    public GetSequenceRoute(final String s,
                            final SequenceController aSequenceController)
    {
        super(s);
        theSequenceController = aSequenceController;
    }

    @Override
    public Object handle(final Request request, final Response response)
    {
        return theSequenceController.getNumber();
    }
}
