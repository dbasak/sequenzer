package com.sequenzer.route;

import com.sequenzer.*;
import org.json.*;
import spark.*;

public class CreateRoute extends Route {
    private final SequenceController theSequenceController;

    public CreateRoute(final String s,
                       final SequenceController aSequenceController)
    {
        super(s);
        theSequenceController = aSequenceController;
    }

    @Override
    public Object handle(final Request request, final Response response)
    {
         theSequenceController.create();

        return "ok";

    }
}
