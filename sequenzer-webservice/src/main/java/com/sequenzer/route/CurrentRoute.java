package com.sequenzer.route;

import com.sequenzer.*;
import spark.*;

public class CurrentRoute extends Route {
    private final SequenceController theSequenceController;

    public CurrentRoute(final String s,
                        final SequenceController aSequenceController)
    {
        super(s);
        theSequenceController = aSequenceController;
    }

    @Override
    public Object handle(final Request request, final Response response)
    {
        return theSequenceController.currentValue();
    }
}
