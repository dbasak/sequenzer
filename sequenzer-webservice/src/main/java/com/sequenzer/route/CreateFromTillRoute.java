package com.sequenzer.route;

import com.sequenzer.*;
import spark.*;

import static java.lang.Long.parseLong;

public class CreateFromTillRoute extends Route {
    private final SequenceController theSequenceController;

    public CreateFromTillRoute(final String s,
                               final SequenceController aSequenceController)
    {
        super(s);
        theSequenceController = aSequenceController;
    }

    @Override
    public Object handle(final Request request, final Response response)
    {
        theSequenceController.create(parseLong(request.params("from")),
                                     parseLong(request.params("till")));
        return "ok";
    }
}
