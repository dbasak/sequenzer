package com.sequenzer;

import java.util.*;

public class SequenceStructure {

    private Set<Long> theNumbers;
    private Long theLastSequence;
    private Long theCurrentSequence;

    public SequenceStructure(final Set<Long> aNumbers,
                             final Long aLastSequence,
                             final Long aCurrentSequence)
    {
        theNumbers = aNumbers;
        theLastSequence = aLastSequence;
        theCurrentSequence = aCurrentSequence;
    }

    public Set<Long> getNumbers()
    {
        return theNumbers;
    }

    public void setNumbers(final Set<Long> aNumbers)
    {
        theNumbers = aNumbers;
    }

    public Long getLastSequence()
    {
        return theLastSequence;
    }

    public void setLastSequence(final Long aLastSequence)
    {
        theLastSequence = aLastSequence;
    }

    public Long getCurrentSequence()
    {
        return theCurrentSequence;
    }

    public void setCurrentSequence(final Long aCurrentSequence)
    {
        theCurrentSequence = aCurrentSequence;
    }
}
