package com.sequenzer;

import java.util.*;

public class Sequences {
    private Sequences()
    {
        //ignore
    }

    public static SequenceController newSequence()
    {
        return new SequenceController(new SequenceStructure(new HashSet<Long>(), 0L, 0L));
    }
}
