package com.sequenzer;

import java.util.*;

public class SequenceController {

    private SequenceStructure theLongs;

    public SequenceController(final SequenceStructure aLongs)
    {
        theLongs = aLongs;
    }

    public long getNumber()
    {
        Long toBeReturned = theLongs.getCurrentSequence();
        theLongs.getNumbers().remove(toBeReturned);

        if(theLongs.getNumbers().iterator().hasNext())
        {
            theLongs.setCurrentSequence(theLongs
                                                .getNumbers()
                                                .iterator()
                                                .next());
        }
        else
        {
            create(theLongs.getLastSequence()+1,
                   theLongs.getLastSequence()+2);

        }



        return toBeReturned;
    }

    public List<Long> getNumbers(int aSize)
    {
        List<Long> myNumbers = new ArrayList<Long>(aSize);
        for(long l=0; l < aSize; l++)
        {
            Long myNext = theLongs.getNumbers().iterator().next();
            myNumbers.add(myNext);
            theLongs.getNumbers().remove(myNext);
        }

        return myNumbers;
    }

    public void create()
    {
        if(theLongs.getNumbers().size() == 0)
        {
            for(long l=0; l < 1000; l++)
            {
                Set<Long> myNumbers = theLongs.getNumbers();
                myNumbers.add(l);
                theLongs.setNumbers(myNumbers);
                theLongs.setCurrentSequence(0L);
                theLongs.setLastSequence(999L);
            }
        }
        else
        {
            Long myLastSequence = theLongs.getLastSequence();
            for(long l= myLastSequence+1; l < myLastSequence +1000; l++)
            {
                Set<Long> myNumbers = theLongs.getNumbers();
                myNumbers.add(l);
                theLongs.setNumbers(myNumbers);
                theLongs.setLastSequence(myLastSequence + 999L);
            }

        }

    }

    public void create(long from, long till)
    {
        for(long l=from; l <= till; l++)
        {
            Set<Long> myNumbers = theLongs.getNumbers();
            myNumbers.add(l);
            theLongs.setNumbers(myNumbers);
            theLongs.setCurrentSequence(from);
            theLongs.setLastSequence(till);
        }
    }

    public long currentValue()
    {
        return theLongs.getCurrentSequence();

    }

    public long lastValue()
    {
        return theLongs.getLastSequence();

    }

    public Set<Long> seeAllValues()
    {
        return theLongs.getNumbers();
    }
}
